package com.photocontest.service;

import com.photocontest.exceptions.*;
import com.photocontest.models.*;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.repositories.contracts.ContestRepository;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.ContestServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.photocontest.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTest {

    private static final int MOCK_ID_0 = 0;
    private static final int MOCK_ID_1 = 1;
    private static final int MOCK_ID_2 = 2;
    private static final String MOCK_RANK_JUNKIE = "Junkie";
    private static final String MOCK_RANK_ADMIN = "Admin";
    private static final String MOCK_CONTEST_CATEGORY_WATERFALL = "Waterfall";
    private static final String MOCK_CONTEST_CATEGORY_NATURE = "Nature";
    private static final String MOCK_DATE_PATTERN = "yyyy-MM-dd";
    private static final String MOCK_DATE_HOURS_PATTERN = "yyyy-MM-dd HH:mm";
    private static final String MOCK_DATE_ONE = "27-04-21";
    private static final String MOCK_DATE_TWO = "22-04-21";
    private static final String MOCK_DATE_HOUR_ONE = "28-05-21 04:51";
    private static final String MOCK_DATE_HOUR_TWO = "28-05-21 04:20";
    private static final String MOCK_DATE_THREE = "27-04-21";
    private static final String MOCK_DATE_FOUR = "27-06-21";
    private static final String MOCK_DATE_HOUR_THREE = "28-05-21 04:51";
    private static final String MOCK_DATE_HOUR_FOUR = "28-07-21 04:51";

    @Mock
    ContestRepository mockContestRepository;
    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    ContestServiceImpl service;

    User mockUser;
    Contest mockContest;
    List<Contest> contestList;
    Image mockImage;
    Review mockReview;
    ContestDto mockContestDto;

    @BeforeEach
    public void setup() throws ParseException {
        mockUser = createMockUser();
        mockContest = createMockContest();
        mockUser.setRanks(Set.of(new Rank(MOCK_ID_1, MOCK_RANK_ADMIN)));
        contestList = new ArrayList<>();
        contestList.add(mockContest);
        mockImage = createMockImage();
        mockReview = createMockReview();
        mockContestDto = new ContestDto();
    }

    @Test
    public void getAll_Should_Return_ContestList_When_CallRepository() {
        //Arrange
        Mockito.when(mockContestRepository.getAll()).thenReturn(contestList);

        //Act
        List<Contest> result = service.getAll(mockUser);

        //Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getById_Should_Return_Contest_When_Matched() {
        // Arrange
        mockUser.setRanks(Set.of(new Rank(MOCK_ID_2, MOCK_RANK_JUNKIE)));
        Mockito.when(mockContestRepository.getById(Mockito.anyInt())).thenReturn(mockContest);

        //Act
        Contest result = service.getById(1, mockUser);

        // Assert
        Assertions.assertEquals(mockContest.getId(), result.getId());
        Assertions.assertEquals(mockContest.getTitle(), result.getTitle());
    }

    @Test
    public void getByPhaseOne_Should_ReturnListContest_When_ValidParameter() {
        // Arrange
        Mockito.when(service.getByPhaseOne(Mockito.anyString(), mockUser)).thenReturn(contestList);

        //Act
        List<Contest> result = service.getByPhaseOne(Mockito.anyString(), mockUser);

        //Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getByPhaseOne_Should_Throw_When_ListIsEmpty() {
        // Arrange
        Mockito.when(service.getByPhaseOne(Mockito.anyString(), mockUser)).thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getByPhaseOne(MOCK_CONTEST_CATEGORY_NATURE, mockUser));
    }

    @Test
    public void getByPhase_Should_Throw_When_ContestNotFound() {
        // Arrange
        Mockito.when(service.getByPhaseOne(Mockito.anyString(), mockUser)).thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getByPhase(Optional.of(MOCK_CONTEST_CATEGORY_NATURE)));
    }

    @Test
    public void getByPhase_Should_Return_When_ContestMach() {
        // Arrange
        Mockito.when(service.getByPhase(Optional.of(Mockito.anyString()))).thenReturn(contestList);

        //Act
        List<Contest> result = service.getByPhase(Optional.of(MOCK_CONTEST_CATEGORY_NATURE));

        //Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void filter_Should_ReturnListContest_When_ValidParameter() {
        //Act
        service.filter(Optional.of(1), Optional.of(MOCK_CONTEST_CATEGORY_WATERFALL), mockUser);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .filter(Optional.of(1), Optional.of(MOCK_CONTEST_CATEGORY_WATERFALL));
    }

    @Test
    public void create_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Mockito.when(mockContestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        service.create(mockContest, mockUser);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .create(Mockito.any(Contest.class));
    }


    @Test
    public void create_Should_Throw_When_Phase1FinisDateIsSetBeforeStartDate() throws ParseException {
        //Arrange
        mockContest.setStartDate(new SimpleDateFormat(MOCK_DATE_PATTERN).parse(MOCK_DATE_ONE));
        mockContest.setFinishDate(new SimpleDateFormat(MOCK_DATE_PATTERN).parse(MOCK_DATE_TWO));

        //Act, Assert
        Assertions.assertThrows(InvalidTimePeriodException.class,
                () -> service.create(mockContest, mockUser));
    }

    @Test
    public void create_Should_Throw_When_Phase2FinishHourIsBeforeStartHour() throws ParseException {
        //Arrange
        mockContest.setPhase2FromHour(new SimpleDateFormat(MOCK_DATE_HOURS_PATTERN).parse(MOCK_DATE_HOUR_ONE));
        mockContest.setPhase2ToHour(new SimpleDateFormat(MOCK_DATE_HOURS_PATTERN).parse(MOCK_DATE_HOUR_TWO));

        //Act, Assert
        Assertions.assertThrows(InvalidTimePeriodException.class,
                () -> service.create(mockContest, mockUser));
    }

    @Test
    public void create_Should_Throw_When_Phase1IsMoreThanOneMount() throws ParseException {
        //Arrange
        mockContest.setStartDate(new SimpleDateFormat(MOCK_DATE_PATTERN).parse(MOCK_DATE_THREE));
        mockContest.setFinishDate(new SimpleDateFormat(MOCK_DATE_PATTERN).parse(MOCK_DATE_FOUR));

        //Act, Assert
        Assertions.assertThrows(InvalidTimePeriodException.class,
                () -> service.create(mockContest, mockUser));
    }

    @Test
    public void create_Should_Throw_When_Phase2ISMoreThanOneDay() throws ParseException {
        //Arrange
        mockContest.setPhase2FromHour(new SimpleDateFormat(MOCK_DATE_HOURS_PATTERN).parse(MOCK_DATE_HOUR_THREE));
        mockContest.setPhase2ToHour(new SimpleDateFormat(MOCK_DATE_HOURS_PATTERN).parse(MOCK_DATE_HOUR_FOUR));

        //Act, Assert
        Assertions.assertThrows(InvalidTimePeriodException.class,
                () -> service.create(mockContest, mockUser));
    }


    @Test
    public void create_Should_Throw_When_ContestWithSameNameExist() {
        //Arrange
        Mockito.when(mockContestRepository.getByTitle(Mockito.anyString())).thenReturn(mockContest);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockContest, mockUser));
    }


    @Test
    public void update_Should_CallRepository_When_ValidParameters() {
        //Act
        service.update(mockContest, mockUser);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(Mockito.any(Contest.class));
    }


    @Test
    public void delete_Should_Throw_When_ContestContainsImages() {
        //Assert
        mockContest.setImages(Set.of(createMockImage()));
        Mockito.when(mockContestRepository.getAll()).thenReturn(contestList);

        //Act, Assert
        Assertions.assertThrows(RuleDeleteException.class,
                () -> service.delete(1, mockUser));
    }

    @Test
    public void delete_Should_CallRepository_When_ContestContainsImages() {
        //Assert
        Mockito.when(mockContestRepository.getAll()).thenReturn(contestList);

        //Act
        service.delete(MOCK_ID_0, mockUser);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .delete(MOCK_ID_0);
    }

    @Test
    public void addUserToContest_Should_CallRepository_When_ValidParameters() {
        //Act
        service.addUserToContest(mockUser, mockContest);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }


    @Test
    public void addJuryToContest_Should_ReturnUser_When_ValidParameters() {
        //Act
        User result = service.addJuryToContest(mockUser, mockContest);

        //Assert
        Assertions.assertEquals(mockUser.getId(), result.getId());
        Assertions.assertEquals(mockUser.getUsername(), result.getUsername());
    }

    @Test
    public void getContestUserSet_Should_ReturnUserSet_When_ValidParameters() {
        //Arrange
        mockContest.setUsers(Set.of(mockUser));
        //Act
        Set<User> result = service.getContestUserSet(mockUser, mockContest);

        //Assert
        Assertions.assertEquals(mockContest.getUsers().size(), result.size());
    }

    @Test
    public void getContestJurySet_Should_ReturnJurySet_When_ValidParameters() {
        //Arrange
        mockContest.setJury(Set.of(mockUser));
        //Act
        Set<User> result = service.getContestJurySet(mockUser, mockContest);

        //Assert
        Assertions.assertEquals(mockContest.getJury().size(), result.size());
    }


//    @Test
//    public void addJury_Should_CallRepository_WhenValidParameters() {
//        //Arrange
//        List<User> users = new ArrayList<>();
//        users.add(mockUser);
//        ContestDto mockContestDto = new ContestDto();
//
//        Mockito.when(mockUserRepository.getAdminList(Mockito.anyString())).thenReturn(users);
//        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);
//
//        //Act
//        service.addJury(mockContestDto, mockContest);
//
//        //Assert
//        Assertions.assertTrue(mockContestDto.getJurySet().isEmpty());
//    }


    @Test
    public void checkIfCanBeJury_Should_Throw_When_UserCannotBeJury() {
        // Arrange
        mockUser.setRanks(Set.of(new Rank(MOCK_ID_2, MOCK_RANK_JUNKIE)));

        // Act, Assert
        Assertions.assertThrows(InvalidDtoDataException.class,
                () -> service.checkIfCanBeJury(mockUser));
    }

    @Test
    public void addParticipants_Should_CallRepository_When_ValidParameter() {
        // Arrange
        mockContestDto.setJurySet(String.valueOf(MOCK_ID_1));
        mockContestDto.setUserSet(String.valueOf(MOCK_ID_1));

        Mockito.when(mockUserRepository.getAdminList(MOCK_RANK_ADMIN))
                .thenReturn(List.of(mockUser));

        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.addParticipants(mockContestDto, mockContest);

        // Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void checkIfJuryUserIsSelectedToParticipants_Should_CallRepository_WhenUserIsJury() {
        //Act
        service.checkIfJuryUserIsSelectedToParticipants(mockContest);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void addImageToContest_Should_CallRepository_When_ValidParameter() {
        //Act
        service.addImageToContest(mockContest, mockImage);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void checkIfUserIsRateImage_Should_Throw_When_UserAlreadyRatePhoto() {
        //Assert
        mockReview.setUser(mockUser);
        mockImage.setReviews(Set.of(mockReview));

        //Act, Assert
        Assertions.assertThrows(ЕvaluationCheckPictureException.class,
                () -> service.checkIfUserIsRateImage(mockUser, mockImage));
    }


    @Test
    public void checkIfUserAlreadyUploadPhoto_Should_CallRepository_WhenAddImageToContest() {
        //Arrange
        mockImage.setUsers(List.of(mockUser));
        mockContest.setImages(Set.of(mockImage));
        mockContest.setUsers(Set.of(mockUser));

        //Act
        service.checkIfUserAlreadyUploadPhoto(mockContest, mockUser);

        //Assert
        Assertions.assertTrue(service.checkIfUserAlreadyUploadPhoto(mockContest, mockUser));
    }


    @Test
    public void getImageReviewsUsers_Should_ReturnMap_When_ValidParameter() {
        //Arrange
        mockImage.setReviews(Set.of(createMockReview()));
        mockContest.setImages(Set.of(mockImage));


        //Act
        Map<Integer, User> result = service.getImageReviewsUsers(mockContest, mockUser);

        //Assert
        Assertions.assertTrue(result.containsValue(mockUser));

    }


}



