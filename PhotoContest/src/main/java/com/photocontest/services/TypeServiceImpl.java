package com.photocontest.services;

import com.photocontest.models.Type;
import com.photocontest.repositories.contracts.TypeRepository;
import com.photocontest.services.contracts.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    private final TypeRepository typeRepository;

    @Autowired
    public TypeServiceImpl(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    @Override
    public List<Type> getAll() {
        return typeRepository.getAll();
    }

    @Override
    public Type getById(int id) {
        return typeRepository.getById(id);
    }

    @Override
    public Type getByName(String name) {
        return typeRepository.getByName(name);
    }
}
