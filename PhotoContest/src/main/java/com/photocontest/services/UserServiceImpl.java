package com.photocontest.services;

import com.photocontest.exceptions.AuthorisationException;
import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Contest;
import com.photocontest.models.DTO.UpdatePasswordDto;
import com.photocontest.models.Image;
import com.photocontest.models.Rank;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.Constants;
import com.photocontest.utils.PaginationHelper;
import com.photocontest.utils.VerifyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.photocontest.utils.Constants.WRONG_CONFIRMATION_PASSWORD;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final ContestService contestService;
    private final PasswordEncoder passwordEncoder;
    private final PaginationHelper paginationHelper;

    @Autowired
    public UserServiceImpl(UserRepository repository,
                           ContestService contestService, PasswordEncoder passwordEncoder,
                           PaginationHelper paginationHelper) {
        this.repository = repository;
        this.contestService = contestService;
        this.passwordEncoder = passwordEncoder;
        this.paginationHelper = paginationHelper;
    }

    @Override
    public List<User> getAll(User user) {

        VerifyHelper.verifyIfAdmin(user);

        return repository.getAll();
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public User getByFirstName(String firstName) {
        return repository.getByFirsName(firstName);
    }

    @Override
    public User getByLastName(String lastName) {
        return repository.getByLastName(lastName);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public User getById(int id, User user) {

        VerifyHelper.verifyIfAnonymous(user);

        if (id != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.USERS_VISIBLE_DATA_MESSAGE);
        }

        return repository.getById(id);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;

        try {
            repository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        repository.create(user);
    }

    @Override
    public void update(User user, User userToUpdate) {

        VerifyHelper.verifyIfAnonymous(user);

        boolean duplicateExists = true;

        try {
            User existingCustomer = repository.getByUsername(user.getUsername());
            if (existingCustomer.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "name", user.getFirstName() + " " + user.getLastName());
        }

        if (userToUpdate.getId() != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.MODIFY_USERS_MESSAGE);
        }

        repository.update(userToUpdate);
    }

    @Override
    public void delete(int id, User user) {

        VerifyHelper.verifyIfAnonymous(user);

        User userToDelete = repository.getById(id);


        if (id != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.MODIFY_USERS_MESSAGE);
        }

        repository.delete(id);
    }

    @Override
    public List<User> searchByMultiple(Optional<String> firstName,
                                       Optional<String> lastName,
                                       Optional<String> email,
                                       Optional<String> username,
                                       User user) {

        VerifyHelper.verifyIfAdmin(user);

        return repository.searchByMultiple(firstName, lastName, email, username);
    }

    @Override
    public List<User> searchByOneWord(Optional<String> oneWord, User user) {

        VerifyHelper.verifyIfAdmin(user);

        return repository.searchByOneWord(oneWord);
    }

    @Override
    public Long getCustomerCount() {
        return repository.getCustomerCount();
    }

    @Override
    public Rank addRank(User user, Rank rank) {

        Set<Rank> newList = user.getRanks();

        newList.add(rank);
        user.setRanks(newList);

        repository.update(user);

        return rank;
    }

    @Override
    public Set<Contest> getUserContests(int userId, User user) {
        VerifyHelper.verifyIfAnonymous(user);

        User currentUser = repository.getById(userId);

        if (userId != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.ERROR_NO_RIGHT);
        }

        return currentUser.getContests();
    }

    @Override
    public void passwordUpdate(UpdatePasswordDto updateData, User loggedUser) {
        updateData.setOldPassword(loggedUser.getPassword());

        checkIfCurrentPasswordIsCorrect(loggedUser.getPassword(), updateData.getOldPassword());
        String newPassword = passwordEncoder.encode(updateData.getNewPassword());
        loggedUser.setPassword(newPassword);
        repository.update(loggedUser);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable, List<User> allUsers) {
        return paginationHelper.findPaginated(pageable, allUsers);
    }


    private void checkIfCurrentPasswordIsCorrect(String currentPassword, String passwordForValidation) {
        if (passwordEncoder.matches(passwordForValidation, currentPassword)) {
            throw new AuthorisationException(WRONG_CONFIRMATION_PASSWORD);
        }
    }

    @Override
    public List<Contest> getUserActiveContests(User currentUser) {
        VerifyHelper.verifyIfAnonymous(currentUser);

        List<Contest> phase1List = contestService.getByPhase(Optional.of("Phase 1"));

        List<Contest> phase2List = contestService.getByPhase(Optional.of("Phase 2"));

        List<Contest> contestList = new LinkedList<>();

        contestList.addAll(phase1List);

        contestList.addAll(phase2List);

        List<Contest> userList = new LinkedList<>();

        for (Contest contest : contestList) {
            if (currentUser.getContests().contains(contest)) {
                userList.add(contest);
            }
        }

        return userList;
    }

    @Override
    public List<Contest> getUserFinishedContests(User currentUser) {
        VerifyHelper.verifyIfAnonymous(currentUser);

        List<Contest> finished = contestService.getByPhase(Optional.of("Finished"));

        List<Contest> userList = new LinkedList<>();

        for (Contest contest : finished) {
            if (currentUser.getContests().contains(contest)) {
                userList.add(contest);
            }
        }

        return userList;
    }

    @Override
    public Image addImageToUser(User user, Image image) {

        Set<Image> newList = user.getImages();

        newList.add(image);
        user.setImages(newList);
        repository.update(user);

        return image;
    }
}