package com.photocontest.services.contracts;

import com.photocontest.models.Phase;

import java.util.List;

public interface PhaseService {
    List<Phase> getAll();

    Phase getById(int id);

    Phase getByName(String name);
}
