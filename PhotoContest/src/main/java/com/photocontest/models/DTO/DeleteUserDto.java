package com.photocontest.models.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class DeleteUserDto {

    @NotNull
    private Integer id;

    private String password;

    public DeleteUserDto(@NotNull Integer id) {
        this.id = id;
    }
}
