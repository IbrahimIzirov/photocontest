package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDto {

    @NotNull(message = Constants.CATEGORY_NULL_MESSAGE)
    @Size(min = 2, max = 30, message = Constants.CATEGORY_NAME_MESSAGE)
    private String name;


    public CategoryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
