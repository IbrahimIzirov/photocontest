package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PhaseDto {

    @NotNull(message = Constants.PHASE_NAME_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.PHASE_NAME_MESSAGE)
    private String name;

    public PhaseDto() {
    }

    public PhaseDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
