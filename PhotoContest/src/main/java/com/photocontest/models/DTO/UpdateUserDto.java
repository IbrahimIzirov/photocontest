package com.photocontest.models.DTO;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UpdateUserDto {

    private Integer id;

    @Size(min = 4, max = 30, message = "Username must be between 4 and 30 characters!")
    private String username;

    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;

    @NotEmpty
    private String password;

    @Email
    private String email;

    private String picture;

    private boolean isActive;
}
