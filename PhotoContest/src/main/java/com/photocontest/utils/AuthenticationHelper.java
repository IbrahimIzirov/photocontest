package com.photocontest.utils;

import com.photocontest.exceptions.AuthenticationFailureException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.User;
import com.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

import static com.photocontest.utils.Constants.NO_LOGGED_IN_USER_MESSAGE;
import static com.photocontest.utils.Constants.WRONG_CONFIRMATION_PASSWORD;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authentication";

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationHelper(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constants.RESOURCE_REQUIRES_AUTHENTICATION);
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constants.INVALID_EMAIL);
        }
    }

    public void verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            checkIfCurrentPasswordIsCorrect(password, user.getPassword());
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(Constants.WRONG_USERNAME_OR_PASSWORD_MESSAGE);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUsername = (String) session.getAttribute(Constants.CURRENT_USERNAME_MESSAGE);

        if (currentUsername == null) {
            throw new UnauthorizedOperationException(Constants.NO_LOGGED_IN_USER_MESSAGE);
        }

        try {
            return userService.getByUsername(currentUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(NO_LOGGED_IN_USER_MESSAGE);
        }
    }

    private void checkIfCurrentPasswordIsCorrect(String currentPassword, String passwordForValidation) {
        if (passwordEncoder.matches(passwordForValidation, currentPassword)) {
            throw new AuthenticationFailureException(WRONG_CONFIRMATION_PASSWORD);
        }
    }
}
