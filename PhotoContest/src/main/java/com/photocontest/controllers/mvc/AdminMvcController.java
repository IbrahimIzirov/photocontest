package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.DTO.*;
import com.photocontest.models.User;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.RankService;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import com.photocontest.utils.VerifyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class AdminMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final RankService rankService;
    private final CategoryService categoryService;

    @Autowired
    public AdminMvcController(UserService userService,
                              AuthenticationHelper authenticationHelper,
                              ContestService contestService,
                              RankService rankService,
                              CategoryService categoryService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.rankService = rankService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public String showAdminProfile(Model model, HttpSession session) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("isPersonal", true);

            model.addAttribute("updateUserDto", new UpdateUserDto());
            model.addAttribute("updatePasswordDto", new UpdatePasswordDto());
            model.addAttribute("deleteUserDto", new DeleteUserDto());
            model.addAttribute("updateContestDto", new UpdateContestDto());
            model.addAttribute("imageDto", new ImageDto());

            VerifyHelper.verifyIfAdmin(currentUser);

            model.addAttribute("contestAll", contestService.getAll(currentUser));
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("contestDto", new ContestDto());
            model.addAttribute("usersAll", userService.getAll(currentUser));
            model.addAttribute("ranksAll", rankService.getAll());


            return "admin-page";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }
}
