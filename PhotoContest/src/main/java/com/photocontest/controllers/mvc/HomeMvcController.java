package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.*;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.DTO.ImageDto;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.PhaseService;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import com.photocontest.utils.VerifyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ImageService imageService;
    private final ContestService contestService;
    private final PhaseService phaseService;

    @Autowired
    public HomeMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             ImageService imageService,
                             PhaseService phaseService,
                             ContestService contestService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.imageService = imageService;
        this.contestService = contestService;
        this.phaseService = phaseService;
    }


    @GetMapping
    public String showHomePage(Model model, HttpSession session) {

        model.addAttribute("imageDto", new ImageDto());
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute(Constants.CURRENT_USER, currentUser);

        } catch (UnauthorizedOperationException e) {
            model.addAttribute(Constants.CURRENT_USER, null);
        }
        return Constants.HOME_PAGE;
    }

    @GetMapping("/about")
    public String showAboutUsPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute(Constants.CURRENT_USER, currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute(Constants.CURRENT_USER, null);
        }
        return Constants.ABOUT_US_PAGE;
    }


    private User getUser() {
        User user = new User("testUser", "1234", "Ivan",
                "Ivanov", "ivanov@yahoo.com", 0);
        user.setRanks(Set.of(new Rank(1, "Admin")));
        return user;
    }



    @ModelAttribute("countUser")
    public long getUserCount() {
        return userService.getCustomerCount();
    }

    @ModelAttribute("winnerImages")
    public List<Image> populateImages() {
        return imageService.getWinnerImages();
    }

    @ModelAttribute("contests")
    public List<Contest> populateContests() {
        Phase phase = phaseService.getById(2);
        return contestService.getByPhaseOne(phase.getName(), getUser());
    }

    private List<Contest> getAllContests() {
        return contestService.getAll(getUser());
    }
}
