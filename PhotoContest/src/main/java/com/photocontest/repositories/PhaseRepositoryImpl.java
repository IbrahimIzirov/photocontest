package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Phase;
import com.photocontest.repositories.contracts.PhaseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhaseRepositoryImpl implements PhaseRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public PhaseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Phase> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Phase", Phase.class)
                    .getResultList();
        }
    }

    @Override
    public Phase getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Phase phase = session.get(Phase.class, id);
            if (phase == null) {
                throw new EntityNotFoundException("Phase", id);
            }
            return phase;
        }
    }

    @Override
    public Phase getByName(String name) {
        try (Session session = sessionFactory.openSession()) {

            Query<Phase> query = session.createQuery("from Phase where name =:name", Phase.class);
            query.setParameter("name", name);

            List<Phase> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Phase", "name", name);
            }
            return result.get(0);
        }
    }
}
