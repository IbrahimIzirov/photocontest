package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from User ", User.class);

            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return users.get(0);
        }
    }


    @Override
    public User getByFirsName(String firstName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where firstName = :firstName", User.class);
            query.setParameter("firstName", firstName);
            List<User> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", firstName);
            }
            return users.get(0);
        }
    }

    @Override
    public User getByLastName(String lastName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where lastName = :lastName", User.class);
            query.setParameter("lastName", lastName);
            List<User> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", lastName);
            }
            return users.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", email);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User user = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> searchByMultiple(Optional<String> firstName,
                                       Optional<String> lastName,
                                       Optional<String> email,
                                       Optional<String> username) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from User c where c.firstName like concat('%', :firstName, '%')  and" +
                    " c.lastName like concat('%', :lastName, '%')  and" +
                    " c.email like concat('%', :email, '%')  and" +
                    " c.username like concat('%', :username, '%')", User.class);

            query.setParameter("firstName", firstName.orElse(""));
            query.setParameter("lastName", lastName.orElse(""));
            query.setParameter("email", email.orElse(""));
            query.setParameter("username", username.orElse(""));

            return query.list();
        }
    }

    @Override
    public List<User> searchByOneWord(Optional<String> oneWord) {
        try (Session session = sessionFactory.openSession()) {
            var query = session
                    .createQuery("from User c where c.username like concat('%', :username, '%')  or" +
                            " c.firstName like concat('%', :firstName, '%')  or" +
                            " c.lastName like concat('%', :lastName, '%')  or" +
                            " c.email like concat('%', :email, '%')", User.class);

            query.setParameter("username", oneWord.orElse(""));
            query.setParameter("firstName", oneWord.orElse(""));
            query.setParameter("lastName", oneWord.orElse(""));
            query.setParameter("email", oneWord.orElse(""));

            return query.list();
        }
    }

    @Override
    public Long getCustomerCount() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("select count(*) from User ", Long.class);

            return query.uniqueResult();
        }
    }

    @Override
    public List<User> getAdminList(String type) {
        try (Session session = sessionFactory.openSession()) {
            var query = session
                    .createQuery("select c from User c join c.ranks as r where r.name =: type", User.class);

            query.setParameter("type", type);

            return query.list();
        }
    }
}