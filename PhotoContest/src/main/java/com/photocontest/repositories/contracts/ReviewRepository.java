package com.photocontest.repositories.contracts;

import com.photocontest.models.Review;

import java.util.List;

public interface ReviewRepository {

    List<Review> getAll();

    Review getById(int id);

    void create(Review review);

    void update(Review review);
}
