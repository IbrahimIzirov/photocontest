package com.photocontest.repositories.contracts;

import com.photocontest.models.Image;

import java.util.List;
import java.util.Optional;

public interface ImageRepository {
    List<Image> getAll();

    Image getById(int id);

    void create(Image image);

    List<Image> searchByTitleOrStory(Optional<String> title,
                                     Optional<String> story);

    void update(Image image);

    List<Image> getWinnerImages();

}
