package com.photocontest.repositories.contracts;

import com.photocontest.models.Type;

import java.util.List;

public interface TypeRepository {

    List<Type> getAll();

    Type getById(int id);

    Type getByName(String name);
}
