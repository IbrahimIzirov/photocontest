package com.photocontest.mappers;

import com.photocontest.models.DTO.RegisterDto;
import com.photocontest.models.DTO.UpdateUserDto;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.RankRepository;
import com.photocontest.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class RegisterMapper {

    private final String DEFAULT_ROLE = "Junkie";

    private final UserRepository userRepository;
    private final RankRepository rankRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegisterMapper(UserRepository userRepository,
                          RankRepository rankRepository,
                          PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.rankRepository = rankRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public User fromDto(RegisterDto registerDto, User user) {
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setUsername(registerDto.getUsername());
        String newPassword = passwordEncoder.encode(registerDto.getPassword());
        user.setPassword(newPassword);
        user.setPicture("https://lh3.googleusercontent.com/proxy/RWkLDo047OKgjD-EIPco6g8KggFHZN1_SpWjjBNCod9HFsW19QswbKg6LOpx5JczKZk01bW-DycEnhwWTS1_d6hq58aQBUeuwxD-gr0WRTJ3q90rCiiffJw");
        return user;
    }

    public User updateMethod(UpdateUserDto updateUserDto, User user) {
        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setEmail(updateUserDto.getEmail());
        user.setUsername(updateUserDto.getUsername());
        user.setPicture(updateUserDto.getPicture());

        return user;
    }
}
