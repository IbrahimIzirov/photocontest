package com.photocontest.controllers.rest;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Category;
import com.photocontest.models.User;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryController(CategoryService categoryService,
                              AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Category> getAll(@RequestHeader HttpHeaders headers) {

        try {
            User users = authenticationHelper.tryGetUser(headers);
            return categoryService.getAll();

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Category getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            return categoryService.getById(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/search")
    public Category getByName(@RequestParam(required = false) Optional<String> name) {
        try {
            return categoryService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
