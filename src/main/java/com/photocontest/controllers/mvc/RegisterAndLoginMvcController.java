package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.AuthenticationFailureException;
import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.RegisterMapper;
import com.photocontest.models.DTO.ForgottenPasswordDto;
import com.photocontest.models.DTO.LoginDto;
import com.photocontest.models.DTO.RegisterDto;
import com.photocontest.models.Rank;
import com.photocontest.models.User;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import com.sun.xml.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.UUID;

import static com.photocontest.utils.Constants.RANK_NAME_JUNKIE;

@Controller
@RequestMapping
public class RegisterAndLoginMvcController {

    private final UserService usersService;
    private final AuthenticationHelper authenticationHelper;
    private final RegisterMapper registerMapper;

    @Autowired
    public RegisterAndLoginMvcController(UserService usersService,
                                         AuthenticationHelper authenticationHelper,
                                         RegisterMapper registerMapper) {
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
        this.registerMapper = registerMapper;
    }


    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());
        return Constants.LOGIN_PAGE;
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return Constants.LOGIN_PAGE;
        }
        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUsername", loginDto.getUsername());
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return Constants.LOGIN_PAGE;
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUsername");
        return "redirect:/";
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto, BindingResult bindingResult) {

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password_error", "Password don't match");
        }

        if (registerDto.getPassword().length() < 8) {
            bindingResult.rejectValue("password", "password_length", "Password length must be 8 characters.");
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            User userToRegister = registerMapper.fromDto(registerDto, new User());
            usersService.create(userToRegister);
            usersService.addRank(userToRegister, new Rank(2, RANK_NAME_JUNKIE));
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
        return "redirect:/login";
    }

    //Todo Да се имплементира метод за изпращане на токен за възстановяване на паролата.
//    @PostMapping("/forgot")
//    public String processForgotPasswordForm(Model model, @Valid @ModelAttribute ForgottenPasswordDto forgottenPassword,
//                                            HttpServletRequest request) {
//        User userInfo = usersService.getByEmail(forgottenPassword.getEmail());
//        if (userInfo.getUserAuthorization().getResetToken() != null) {
//            throw new UnauthorizedOperationException("You can reset your password once per hour");
//        }
//        // Generate random 36-character string token for reset password
//
//        userInfo.getUserAuthorization().setResetToken(UUID.randomUUID().toString());
//
//
//        userInfoService.update(userInfo, userInfo);
//
//        String appUrl = "http://" + request.getServerName() + ":8080";
//        String text = "To reset your password, click the link below:" + appUrl
//                + "/users/reset?token=" + userInfo.getUserAuthorization().getResetToken();
//
//        String to = userInfo.getUserAuthorization().getEmail();
//        String subject = ("Password Reset Request");
//        emailService.sendMail(to, subject, text);
//        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
//        executorService.schedule(new TokenScheduler(userInfoService, userInfo), 60, TimeUnit.MINUTES);
//        //Add success message to view
//
//        return "redirect:/";
//
//    }

}
