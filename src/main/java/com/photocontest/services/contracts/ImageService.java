package com.photocontest.services.contracts;

import com.photocontest.models.Image;
import com.photocontest.models.User;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ImageService {
    List<Image> getAll();

    Image getById(int id);

    void create(Image image);

    List<Image> searchByMultiply(Optional<String> title,
                                 Optional<String> story);

    List<Image> getWinnerImages();

    String uploadImageToImgurAPI(String image) throws IOException, JSONException;
}
