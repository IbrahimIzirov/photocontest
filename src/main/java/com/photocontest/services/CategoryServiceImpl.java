package com.photocontest.services;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(Optional<String> name) {
        Category result = null;

        if (name.isPresent()) {
            result = categoryRepository.getByName(name.get());
            if (result == null) {
                throw new EntityNotFoundException("Category", "name", name.get());
            }
        }
        return result;
    }
}


